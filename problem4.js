// ==== Problem #4 ====
// * The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
// const inventory = require("./data/dataSet")

const carYears = (inventory) => {
    let allYears = []
    for (let i = 0; i < inventory.length; i++) {
        allYears.push(inventory[i].car_year)
    }
    console.log(allYears)
    return allYears
}
module.exports = carYears