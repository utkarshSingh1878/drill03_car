// ==== Problem #2 ====
// * The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
"Last car is a *car make goes here* *car model goes here*"

// const inventory = require("./data/dataSet")
// console.log([inventory[inventory.length - 1]])
let res = (inventory) => (inventory[inventory.length - 1])
console.log(
    `Last car is a ${res.car_make} ${res.car_model}`
)
console.log(res)


module.exports = res;