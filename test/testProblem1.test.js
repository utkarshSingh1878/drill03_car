const inventory = require("../data/dataSet")
const carID33 = require("../problem1")
let ans = { id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011 }

let idToSearch = 33

test('should give the data of car with id 33', () => {
    expect(carID33(inventory, idToSearch)).toEqual(ans)
})