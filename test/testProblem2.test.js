const inventory = require("../data/dataSet")
const lastCar = require("../problem2")
let ans = { id: 50, car_make: 'Lincoln', car_model: 'Town Car', car_year: 1999 }
test('should return last car in invetory', () => {
    expect(lastCar(inventory)).toEqual(ans)
})

// console.log(lastCar)