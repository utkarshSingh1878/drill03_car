const inventory = require("../data/dataSet")

const olderCars = require("../problem5")
let myRes = olderCars(inventory).length
// console.log(myRes.length)
const ans = 25

test('should return length of array of cars build before 2000', () => {
    expect(myRes).toBe(ans)
})