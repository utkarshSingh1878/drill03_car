const inventory = require("./data/dataSet")

// ==== Problem #5 ====
// * The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const carsBefore2000 = (inventory) => {
    let result = []
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_year < 2000) {
            result.push(inventory[i])
        }
    }
    return result
}
// let result = carsBefore2000
// // console.log(carsBefore2000.length)

module.exports = carsBefore2000